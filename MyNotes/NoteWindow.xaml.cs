﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyNotes.model;

namespace MyNotes
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class NoteWindow : Window
    {
        Engine engine;
        public int ID;
        internal int col;

        public NoteWindow(Engine engine, int ID)
        {
            InitializeComponent();
            this.engine = engine;
            this.ID = ID;
            col = 0;
            grid1.Background = Brushes.Transparent;
        }

        internal void changeColor()
        {
            Brush tmp = null;
            var bc = new BrushConverter();
            switch (col)
            {
                case 0: tmp = (Brush)bc.ConvertFrom("#FFF8b8"); break;
                case 1: tmp = (Brush)bc.ConvertFrom("#C4F5BF"); break;
                case 2: tmp = (Brush)bc.ConvertFrom("#FF9AA4"); break;
                case 3: tmp = (Brush)bc.ConvertFrom("#CBE9F8"); break;
            }
            this.Background = tmp;
            text.Background = tmp;
        }

        private void text_TextChanged(object sender, TextChangedEventArgs e)
        {
            engine.notifyTextChanged(this.ID);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Size oS = e.PreviousSize;
            Size nS = e.NewSize;
            double hDiff = oS.Height - nS.Height;
            double wDiff = oS.Width - nS.Width;
            text.Width -= wDiff;
            text.Height -= hDiff;
            engine.notifySizeChanged(this.ID);
        }

        private void addLa_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            engine.notifyNewNoteRequest();           
        }

        private void clearLa_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            this.text.Text = "";
            engine.notifyTextChanged(this.ID);        
        }

        private void delLa_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            engine.notifyDeleteRequest(this.ID);
            this.Close();
        }

        private void grid1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
                DragMove();
                engine.notifyPosChange(this.ID);
        }

        private void saveLa_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            engine.kill();
        }

        private void menuBlue_Click(object sender, RoutedEventArgs e)
        {
            this.col = 3;
            changeColor();
            engine.notifyColorChange(this.ID);
        }

        private void menuYellow_Click(object sender, RoutedEventArgs e)
        {
            this.col = 0;
            changeColor();
            engine.notifyColorChange(this.ID);
        }

        private void menuRed_Click(object sender, RoutedEventArgs e)
        {
            this.col = 2;
            changeColor();
            engine.notifyColorChange(this.ID);
        }

        private void menuGreen_Click(object sender, RoutedEventArgs e)
        {
            this.col = 1;
            changeColor();
            engine.notifyColorChange(this.ID);
        }
    }
}
