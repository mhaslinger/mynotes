﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using MyNotes.model;
using System.Windows.Forms;

namespace MyNotes
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : System.Windows.Application
    {
        Engine engine;
        void App_Startup(object sender, StartupEventArgs e)
        {
            // Application is running
            // no command line args

            engine = new Engine();
            engine.doStartup();
           // mainWindow.Show();
            NotifyIcon icon = new NotifyIcon();
           icon.Text = "MyNotes";
           icon.Icon = new System.Drawing.Icon("favicon.ico");
           //icon.Icon = new System.Drawing.Icon("/MyNotes;component/Icons/favicon.ico");
           icon.DoubleClick += new System.EventHandler(this.Icon_DoubleClick);
           icon.Visible = true;
           engine.setTrayIco(icon);
        }

       public void Icon_DoubleClick(object sender, System.EventArgs e)
        {
            engine.goFront();
        }
    }
}
