﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;

namespace MyNotes.model
{
    class XMLProcessor
    {
        String path;
        int cnt;
        Stack<String[]> notes;

        public XMLProcessor(String path)
        {
            this.path = path;
            this.notes = new Stack<String[]>();
            load();
        }

        private void load()
        {
            if (File.Exists(path))
            {
                try
                {
                    XElement root = XElement.Load(path);
                    Console.WriteLine(root.ToString());
                    foreach (XElement element in root.Elements("note"))
                    {
                        String[] tmp = new String[6];
                        XElement x = element.Element("x");
                        XElement y = element.Element("y");
                        XElement text = element.Element("text");
                        XElement col = element.Element("color");
                        XElement w = element.Element("width");
                        XElement h = element.Element("height");
                        tmp[0] = x.Value;
                        tmp[1] = y.Value;
                        tmp[2] = text.Value;
                        tmp[3] = col.Value;
                        tmp[4] = w.Value;
                        tmp[5] = h.Value;
                        this.notes.Push(tmp);
                    }
                    this.cnt = notes.Count;
                }
                catch (Exception e)
                {
                    this.cnt = 0;
                }
            }
            else
            {
                this.cnt = 0;
                File.Create(path).Dispose();
            }
        }

        internal int Count()
        {
            return this.cnt;
        }

        internal Note getNextNote(int id)
        {
            String[] tmp = this.notes.Pop();
            this.cnt--;
            return new Note(id, tmp[2], int.Parse(tmp[0]), int.Parse(tmp[1]),int.Parse(tmp[3]),int.Parse(tmp[4]),int.Parse(tmp[5]));
        }

        internal void saveNotes(Dictionary<int, Note>.ValueCollection valueCollection)
        {
                XmlWriter writer = XmlWriter.Create(path);
                writer.WriteStartDocument();
                writer.WriteStartElement("notes");
                foreach (Note n in valueCollection)
                {
                    writer.WriteStartElement("note");
                    writer.WriteElementString("x", n.x.ToString());
                    writer.WriteElementString("y", n.y.ToString());
                    writer.WriteElementString("text", n.value);
                    writer.WriteElementString("color", n.col.ToString());
                    writer.WriteElementString("width", n.w.ToString());
                    writer.WriteElementString("height", n.h.ToString());
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();            
        }

        private void cleanup()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            //File.Create(path);
        }
    }
}
