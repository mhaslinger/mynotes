﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyNotes.model
{
    class Dic<K,T> : Dictionary<K,T>
    {
        public Dic() : base(){
    
        }

        public T get(K key){
            T value = default(T);
            if (this.ContainsKey(key))
            {
                this.TryGetValue(key, out value);
            }
            return value;
        }
    }
}
