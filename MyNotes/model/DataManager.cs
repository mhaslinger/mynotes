﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyNotes.model
{
    class DataManager
    {
        Dic<int, Note> notes;
        String filepath;
        Stack<int> idStack;
        int idCnt;
        XMLProcessor xmlP;

        public DataManager(String path)
        {
            this.notes = new Dic<int, Note>();
            this.filepath = path;
            this.idStack = new Stack<int>();
            this.idCnt = 1;
            this.xmlP = new XMLProcessor(path);
        }

        internal void load()
        {
            int cnt = this.xmlP.Count();
            for (int i=0; i < cnt; i++)
            {
                int id = getID();
                this.notes.Add(id, this.xmlP.getNextNote(id));
            }
        }

        internal bool isEmpty()
        {
            if (this.notes.Count == 0)
            {
                return true;
            }
            return false;
        }

        internal List<Note> getAll()
        {
            List<Note> ret = new List<Note>();
            foreach (Note n in this.notes.Values){
                ret.Add(n);
            }
            return ret;
        }

        internal int getID()
        {
            if (this.idStack.Count == 0)
            {
                int ret = this.idCnt;
                this.idCnt++;
                return ret;
            }
            else
            {
                return this.idStack.Pop();
            }
        }

        internal void trashNote(int id)
        {
            if (this.notes.ContainsKey(id))
            {
                this.notes.Remove(id);
                this.idStack.Push(id);
            }
        }

        internal void updateNote(Note n)
        {
            if (this.notes.ContainsKey(n.ID))
            {
                this.notes.Remove(n.ID);
                this.notes.Add(n.ID, n);
            }
        }

        internal void addNote(Note n)
        {
            if (n != null && !this.notes.ContainsKey(n.ID))
            {
                this.notes.Add(n.ID, n);
            }
        }

        internal void save()
        {
            this.xmlP.saveNotes(this.notes.Values);
        }
    }
}
