﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Microsoft.Win32;
using System.IO;
using System.Windows.Forms;

namespace MyNotes.model
{
    public class Engine
    {
        List<NoteWindow> notes;
        DataManager dm;
        double sw = System.Windows.SystemParameters.PrimaryScreenWidth;
        double sh = System.Windows.SystemParameters.PrimaryScreenHeight;
        bool changed;
        System.Timers.Timer timer;
        NotifyIcon icon;

        public Engine()
        {
            this.notes = new List<NoteWindow>();
            //String path = Directory.GetCurrentDirectory() + "\\notes.xml";
            String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MyNotes");
            Console.WriteLine(path);
            this.dm = new DataManager(path);
            this.dm.load();
            if (!this.dm.isEmpty())
            {
                foreach (Note n in this.dm.getAll()){
                    createNoteWindow(n);
                }
            }
            else
            {
                createNewNote();
            }
            this.changed = false;
            timer = new System.Timers.Timer();
            timer.Interval = (1000 * 60 * 5);
            //timer.Interval = 1000;
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            timer.Enabled = true;
            SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);
        }

       

        private NoteWindow createNoteWindow(Note n)
        {
            var newWindow = new NoteWindow(this,n.ID);
            this.notes.Add(newWindow);
            newWindow.Left = n.x;
            newWindow.Top = n.y;
            newWindow.text.Text = n.value;
            newWindow.col = n.col;
            newWindow.changeColor();
            newWindow.ShowInTaskbar = false;
            newWindow.Width = n.w;
            newWindow.Height = n.h;
            return newWindow;
        }

        private NoteWindow createNewNote()
        {
            Note tmp = new Note(this.dm.getID());
            tmp.x = (int) sw / 2;
            tmp.y = (int) sh / 2;
            this.dm.addNote(tmp);
            return createNoteWindow(tmp);
        }

        internal void doStartup()
        {
            foreach (NoteWindow w in this.notes)
            {
                w.Show();
            }
        }

        internal void notifyNewNoteRequest()
        {
            createNewNote().Show();
            this.changed = true;
        }

        internal void notifyDeleteRequest(int p)
        {
            foreach (NoteWindow nw in this.notes)
            {
                if (nw.ID == p)
                {
                    this.notes.Remove(nw);
                    break;
                }
            }
            this.dm.trashNote(p);
            this.changed = true;
            if (this.notes.Count == 0) kill();
        }

        internal void notifyTextChanged(int p)
        {
            this.dm.updateNote(getNote(p));
            this.changed = true;
        }

        internal void notifyPosChange(int p)
        {
            notifyTextChanged(p);
            this.changed = true;
        }

        private Note getNote(int id)
        {
            NoteWindow win = null;
            foreach (NoteWindow nw in this.notes)
            {
                if (nw.ID == id)
                {
                    win = nw;
                    break;
                }
            }
            if (win != null)
            {
                Note ret = new Note(id, win.text.Text, (int) win.Left, (int) win.Top, (int) win.col, (int) win.Width, (int) win.Height);
                return ret;
            }
            return null;
        }

        private void processSave()
        {
            if (this.changed)
            {
                this.dm.save();
                this.changed = false;
            }
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            processSave();
        }

        void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            kill();
        }

        internal void kill()
        {
            timer.Dispose();
            SystemEvents.SessionEnding -= new SessionEndingEventHandler(SystemEvents_SessionEnding);
            processSave();
            foreach (NoteWindow nw in this.notes)
            {
                nw.Close();
            }
            icon.DoubleClick -= new System.EventHandler(this.Icon_DoubleClick);
            icon.Dispose();
            App.Current.Shutdown();
        }

        internal void notifyColorChange(int p)
        {
            this.dm.updateNote(getNote(p));
            this.changed = true;
        }

        internal void notifySizeChanged(int p)
        {
            notifyTextChanged(p);
        }

        internal void goFront()
        {
            foreach (NoteWindow nw in this.notes)
            {
                nw.Activate();
            }
        }

        internal void setTrayIco(System.Windows.Forms.NotifyIcon icon)
        {
            this.icon = icon;
        }

        public void Icon_DoubleClick(object sender, System.EventArgs e)
        {
            this.goFront();
        }
    }
}