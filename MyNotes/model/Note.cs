﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyNotes.model
{
    class Note
    {
        internal int ID;
        internal String value;
        internal int x;
        internal int y;
        internal int col;
        internal int w;
        internal int h;

        public Note(int id)
        {
            this.ID = id;
            this.value = "";
            this.x = 1250;
            this.y = 1000;
            this.col = 0;
            this.w = 250;
            this.h = 250;
        }

        public Note(int id, String value, int x, int y,int col,int w, int h)
        {
            this.ID = id;
            this.value = value;
            this.x = x;
            this.y = y;
            this.col = col;
            this.w = w;
            this.h = h;
        }
    }
}
